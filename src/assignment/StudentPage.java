/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author ASUS
 */
public class StudentPage implements ActionListener {

    String studentID, studentName;
    JFrame userPageFrame = new JFrame("PI Tution Centre (PTC)");
    JLabel name = new JLabel("Pi-Tution Centre(PTC)", JLabel.CENTER);
    JPanel panel1 = new JPanel();
    JButton lessons = new JButton("Schedule Lessons");
    JButton books = new JButton("View Books");
    JButton attendLessons = new JButton("Attend/Cancel Lessons");
    JButton viewProfile = new JButton("View My Profile");
    JButton logOut = new JButton("LogOut");

    StudentPage(String studentID, String studentName) {

        this.studentID = studentID;
        this.studentName = studentName;

        userPageFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        userPageFrame.setTitle("Welcome " + studentID + " (" + studentName + ")");
        userPageFrame.setLayout(null);
        userPageFrame.add(panel1);
        userPageFrame.setResizable(false);
        userPageFrame.getContentPane().setBackground(new Color(250, 245, 228));
        userPageFrame.add(name, BorderLayout.CENTER);

        name.setBounds(0, 25, 700, 50);
        name.setFont(new Font("Acumin Variable Concept", Font.BOLD, 24));

        panel1.setLayout(new GridLayout(4, 1, 10, 10));
        panel1.setBounds(150, 100, 400, 400);
        panel1.add(lessons);
        panel1.add(books);
        panel1.add(attendLessons);
        panel1.add(viewProfile);

        lessons.addActionListener(this);
        books.addActionListener(this);
        attendLessons.addActionListener(this);
        viewProfile.addActionListener(this);

        logOut.setBounds(550, 550, 100, 30);
        logOut.setFont(new Font("Acumin Variable Concept", Font.BOLD, 14));
        logOut.addActionListener(this);
        userPageFrame.add(logOut);

        userPageFrame.setSize(700, 650);
        userPageFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == lessons) {
            new ScheduleLessons(studentID, studentName);
            userPageFrame.dispose();
        }
        if (e.getSource() == logOut) {
            new HomePage();
            userPageFrame.dispose();
        }
        if (e.getSource() == books) {
            new BooksPage(studentID, studentName);
            userPageFrame.dispose();
        }
        if (e.getSource() == attendLessons) {
            new attendCancelLessons(studentID, studentName);
            userPageFrame.dispose();
        }
        if (e.getSource() == viewProfile) {
            new myProfileStudent(studentID, studentName);
            userPageFrame.dispose();
        }
    }
}
