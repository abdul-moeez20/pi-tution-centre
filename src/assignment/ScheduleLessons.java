/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class ScheduleLessons implements ActionListener {

    private FetchData fetchData = new FetchData();
    private List<List<String>> bookingData = fetchData.getBookingData();
    private String studentID, studentName;
    private int rowSelected;
    private JFrame LessonsFrame = new JFrame("Schedule Lessons");
    private JLabel messageLabel;
    private JTextField lessonIDField, lessonSubjectField, lessonWeekField,
            lessonDateField, lessonTimeField, lessonTutorField, lessonPriceField;
    private JScrollPane tablePane = new JScrollPane();
    private JTable lessonsTable = new JTable();
    private JButton slotAvailableButton = new JButton("Check if Available");
    private JButton scheduleButton = new JButton("Schedule");
    private JButton backButton = new JButton("Return");
    private int count = 0;

    public ScheduleLessons(String studentID, String studentName) {

        this.studentID = studentID;
        this.studentName = studentName;

        LessonsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        LessonsFrame.setLayout(null);
        LessonsFrame.setResizable(false);
        LessonsFrame.getContentPane().setBackground(new Color(250, 245, 228));

        messageLabel = new JLabel("Welcome " + studentName + ". Please select a Lesson from the Table Below", JLabel.CENTER);
        messageLabel.setBounds(0, 0, 1000, 50);
        LessonsFrame.add(messageLabel, BorderLayout.CENTER);

        tablePane.setBounds(50, 50, 900, 300);
        LessonsFrame.add(tablePane);

        JLabel lessonDetails = new JLabel("Selected Lesson Details:");
        lessonDetails.setBounds(57, 293, 220, 150);

        JLabel box = new JLabel();
        box.setBounds(57, 382, 207, 146);
        box.setBackground(Color.white);
        box.setForeground(Color.white);
        box.setBorder(BorderFactory.createLineBorder(Color.black, 3));

        lessonIDField = new JTextField();
        lessonSubjectField = new JTextField();
        lessonWeekField = new JTextField();
        lessonDateField = new JTextField();
        lessonTimeField = new JTextField();
        lessonTutorField = new JTextField();
        lessonPriceField = new JTextField();

        lessonIDField.setEditable(false);
        lessonSubjectField.setEditable(false);
        lessonWeekField.setEditable(false);
        lessonDateField.setEditable(false);
        lessonTimeField.setEditable(false);
        lessonTutorField.setEditable(false);
        lessonPriceField.setEditable(false);

        lessonIDField.setBounds(60, 385, 200, 20);
        lessonSubjectField.setBounds(60, 405, 200, 20);
        lessonWeekField.setBounds(60, 425, 200, 20);
        lessonDateField.setBounds(60, 445, 200, 20);
        lessonTimeField.setBounds(60, 465, 200, 20);
        lessonTutorField.setBounds(60, 485, 200, 20);
        lessonPriceField.setBounds(60, 505, 200, 20);

        slotAvailableButton.setBounds(750, 375, 200, 30);
        slotAvailableButton.addActionListener(this);

        scheduleButton.setBounds(750, 410, 200, 30);
        scheduleButton.setVisible(false);
        scheduleButton.addActionListener(this);

        backButton.setBounds(850, 500, 100, 30);
        backButton.addActionListener(this);

        lessonsTable.setModel(new DefaultTableModel(
                new Object[][]{
                    //English Lesson List
                    {"ENSAM05", "English", "Week 1", "Saturday March 5th", "Morning", "Mr. Walker", "1000"},
                    {"ENSUM06", "English", "Week 1", "Sunday March 6th", "Morning", "Mr. Walker", "1000"},
                    {"ENSAM12", "English", "Week 2", "Saturday March 12th", "Morning", "Mr. Walker", "1000"},
                    {"ENSUM13", "English", "Week 2", "Sunday March 13th", "Morning", "Mr. Walker", "1000"},
                    {"ENSAM19", "English", "Week 3", "Saturday March 19th", "Morning", "Mr. Walker", "1000"},
                    {"ENSUM20", "English", "Week 3", "Sunday March 20th", "Morning", "Mr. Walker", "1000"},
                    {"ENSAM26", "English", "Week 4", "Saturday March 26th", "Morning", "Mr. Walker", "1000"},
                    {"ENSM207", "English", "Week 4", "Sunday March 27th", "Morning", "Mr. Walker", "1000"},
                    {"ENSAA02", "English", "Week 5", "Saturday April 2nd", "Morning", "Mr. Walker", "1000"},
                    {"ENSUA03", "English", "Week 5", "Sunday April 3rd", "Morning", "Mr. Walker", "1000"},
                    {"ENSAA09", "English", "Week 6", "Saturday April 9th", "Morning", "Mr. Walker", "1000"},
                    {"ENSUA10", "English", "Week 6", "Sunday April 10th", "Morning", "Mr. Walker", "1000"},
                    {"ENSAA16", "English", "Week 7", "Saturday April 16th", "Morning", "Mr. Walker", "1000"},
                    {"ENSUA17", "English", "Week 7", "Sunday April 17th", "Morning", "Mr. Walker", "1000"},
                    {"ENSAA23", "English", "Week 8", "Saturday April 23rd", "Morning", "Mr. Walker", "1000"},
                    {"ENSUA24", "English", "Week 8", "Sunday April 24th", "Morning", "Mr. Walker", "1000"},
                    //Math Lesson List
                    {"MASAM05", "Math", "Week 1", "Saturday March 5th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUM06", "Math", "Week 1", "Sunday March 6th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASAM12", "Math", "Week 2", "Saturday March 12th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUM13", "Math", "Week 2", "Sunday March 13th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASAM19", "Math", "Week 3", "Saturday March 19th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUM20", "Math", "Week 3", "Sunday March 20th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASAM26", "Math", "Week 4", "Saturday March 26th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUM27", "Math", "Week 4", "Sunday March 27th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASAA02", "Math", "Week 5", "Saturday April 2nd", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUA03", "Math", "Week 5", "Sunday April 3rd", "EVENING", "Mr.Oliver", "1500"},
                    {"MASAA09", "Math", "Week 6", "Saturday April 9th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUA10", "Math", "Week 6", "Sunday April 10th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASAA16", "Math", "Week 7", "Saturday April 16th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUA17", "Math", "Week 7", "Sunday April 17th", "EVENING", "Mr.Oliver", "1500"},
                    {"MASAA23", "Math", "Week 8", "Saturday April 23rd", "EVENING", "Mr.Oliver", "1500"},
                    {"MASUA24", "Math", "Week 8", "Sunday April 24th", "EVENING", "Mr.Oliver", "1500"},
                    //Verbal Reasoning Lesson List
                    {"VRSUM06", "Verbal Reasoning", "Week 1", "Sunday March 6th", "AFTERNOON", "Mr.Henry", "1100"},
                    {"VRSUM13", "Verbal Reasoning", "Week 2", "Sunday March 13th", "AFTERNOON", "Mr.Henry", "1100"},
                    {"VRSUM20", "Verbal Reasoning", "Week 3", "Sunday March 20th", "AFTERNOON", "Mr.Henry", "1100"},
                    {"VRSUM27", "Verbal Reasoning", "Week 4", "Sunday March 27th", "AFTERNOON", "Mr.Henry", "1100"},
                    {"VRSUA03", "Verbal Reasoning", "Week 5", "Sunday April 3rd", "AFTERNOON", "Mr.Henry", "1100"},
                    {"VRSUA10", "Verbal Reasoning", "Week 6", "Sunday April 10th", "AFTERNOON", "Mr.Henry", "1100"},
                    {"VRSUA17", "Verbal Reasoning", "Week 7", "Sunday April 17th", "AFTERNOON", "Mr.Henry", "1100"},
                    {"VRSUA24", "Verbal Reasoning", "Week 8", "Sunday April 24th", "AFTERNOON", "Mr.Henry", "1100"},
                    //Non verbal Lesson Reasoning
                    {"NVSAM05", "Nonverbal Reasoning", "Week 1", "Saturday March 5th", "AFTERNOON", "Ms.Emma", "1200"},
                    {"NVSAM12", "Nonverbal Reasoning", "Week 2", "Saturday March 12th", "AFTERNOON", "Ms.Emma", "1200"},
                    {"NVSAM19", "Nonverbal Reasoning", "Week 3", "Saturday March 19th", "AFTERNOON", "Ms.Emma", "1200"},
                    {"NVSAM26", "Nonverbal Reasoning", "Week 4", "Saturday March 26th", "AFTERNOON", "Ms.Emma", "1200"},
                    {"NVSAA02", "Nonverbal Reasoning", "Week 5", "Saturday April 2nd", "AFTERNOON", "Ms.Emma", "1200"},
                    {"NVSAA09", "Nonverbal Reasoning", "Week 6", "Saturday April 9th", "AFTERNOON", "Ms.Emma", "1200"},
                    {"NVSAA16", "Nonverbal Reasoning", "Week 7", "Saturday April 16th", "AFTERNOON", "Ms.Emma", "1200"},
                    {"NVSAA23", "Nonverbal Reasoning", "Week 8", "Saturday April 23rd", "AFTERNOON", "Ms.Emma", "1200"},},
                new String[]{"LessonID", "Subject", "Week", "Date", "Time", "Teacher", "Price"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        }
        );

        lessonsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lessonsTableMouseClicked(evt);
            }
        });

        tablePane.setViewportView(lessonsTable);

        LessonsFrame.add(lessonDetails);
        LessonsFrame.add(box);
        LessonsFrame.add(backButton);
        LessonsFrame.add(lessonIDField);
        LessonsFrame.add(lessonSubjectField);
        LessonsFrame.add(lessonWeekField);
        LessonsFrame.add(lessonDateField);
        LessonsFrame.add(lessonTimeField);
        LessonsFrame.add(lessonTutorField);
        LessonsFrame.add(lessonPriceField);
        LessonsFrame.add(scheduleButton);
        LessonsFrame.add(slotAvailableButton);
        LessonsFrame.setSize(1000, 600);
        LessonsFrame.setVisible(true);

    }

    //MouseClick Event for lessonsTable
    private void lessonsTableMouseClicked(MouseEvent evt) {

        DefaultTableModel tableModel = (DefaultTableModel) lessonsTable.getModel();
        rowSelected = lessonsTable.getSelectedRow();

        lessonIDField.setText(tableModel.getValueAt(rowSelected, 0).toString());
        lessonSubjectField.setText(tableModel.getValueAt(rowSelected, 1).toString());
        lessonWeekField.setText(tableModel.getValueAt(rowSelected, 2).toString());
        lessonDateField.setText(tableModel.getValueAt(rowSelected, 3).toString());
        lessonTimeField.setText(tableModel.getValueAt(rowSelected, 4).toString());
        lessonTutorField.setText(tableModel.getValueAt(rowSelected, 5).toString());
        lessonPriceField.setText(tableModel.getValueAt(rowSelected, 6).toString());
        scheduleButton.setVisible(false);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == slotAvailableButton) {

            for (int i = 0; i < bookingData.size(); i++) {

                if (bookingData.get(i).get(0).equals(lessonIDField.getText())) {
                    count++;
                }

                if (bookingData.get(i).get(0).equals(lessonIDField.getText()) && bookingData.get(i).get(1).equals(studentID)) {
                    JOptionPane.showMessageDialog(null, "Lesson is already Scheduled");
                    new ScheduleLessons(studentID, studentName);
                    LessonsFrame.dispose();
                    break;
                }

            }

            if (count < 4) {
                scheduleButton.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "No Slots Available");
                scheduleButton.setVisible(false);
                count = 0;
            }

            if (lessonIDField.getText().isEmpty()) {
                new ScheduleLessons(studentID, studentName);
                LessonsFrame.dispose();
            }

        }

        if (e.getSource() == scheduleButton) {

            String id = lessonIDField.getText();
            String subject = lessonSubjectField.getText();
            String date = lessonDateField.getText();
            String tutor = lessonTutorField.getText();
            String price = lessonPriceField.getText();

            try {
                FileWriter fw = new FileWriter("programData\\bookings.csv", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);

                pw.println(id + "," + studentID + "," + subject + "," + date
                        + "," + tutor + "," + price + "," + "Booked" + "," + "." + "," + "." + "," + ".");
                pw.flush();
                pw.close();
                JOptionPane.showMessageDialog(null, "Lesson Booked :)");
                new ScheduleLessons(studentID, studentName);

            } catch (IOException ex) {
            }
        }

        if (e.getSource() == backButton) {
            LessonsFrame.dispose();
            new StudentPage(studentID, studentName);

        }

    }

}
