/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author ASUS
 */
public class TeacherPage implements ActionListener {

    String teacherID, teacherName;
    JFrame teacherPageFrame = new JFrame("PI Tution Centre (PTC)");
    JLabel name;
    JPanel panel1 = new JPanel();
    JButton lessons = new JButton("My Lessons");
    JButton notes = new JButton("Add Notes");
    JButton amendBookList = new JButton("Amend Book List");
    JButton monthlyReports = new JButton("Monthly Reports");
    JButton logOut = new JButton("LogOut");

    TeacherPage(String teacherID, String teacherName) {

        this.teacherID = teacherID;
        this.teacherName = teacherName;

        teacherPageFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        teacherPageFrame.setTitle("Welcome " + teacherID);
        teacherPageFrame.setLayout(null);
        teacherPageFrame.add(panel1);
        teacherPageFrame.setResizable(false);

        name = new JLabel("Hello " + teacherName + ". What would you like to do today?", JLabel.CENTER);
        name.setBounds(0, 0, 700, 50);
        name.setFont(new Font("Acumin Variable Concept", Font.BOLD, 24));
        teacherPageFrame.add(name, BorderLayout.CENTER);

        panel1.setLayout(new GridLayout(1, 4, 10, 10));
        panel1.setBounds(15, 70, 650, 50);
        panel1.add(lessons);
        lessons.addActionListener(this);
        amendBookList.addActionListener(this);
        notes.addActionListener(this);
        monthlyReports.addActionListener(this);
        panel1.add(notes);
        panel1.add(amendBookList);
        panel1.add(monthlyReports);

        logOut.setBounds(575, 130, 90, 30);
        logOut.setFont(new Font("Acumin Variable Concept", Font.BOLD, 14));
        logOut.addActionListener(this);

        teacherPageFrame.add(logOut);
        teacherPageFrame.setSize(700, 220);
        teacherPageFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == lessons) {
            new MyLessons(teacherID, teacherName);
            teacherPageFrame.dispose();

        }
        if (e.getSource() == notes) {

            new addNotesTeacher(teacherID, teacherName);
            teacherPageFrame.dispose();

        }
        if (e.getSource() == amendBookList) {
            new AmendBookList(teacherID, teacherName);
            teacherPageFrame.dispose();

        }
        if (e.getSource() == monthlyReports) {
            new monthlyReportTeacher(teacherID, teacherName);
            teacherPageFrame.dispose();

        }
        if (e.getSource() == logOut) {
            new HomePage();
            teacherPageFrame.dispose();
        }
    }
}
