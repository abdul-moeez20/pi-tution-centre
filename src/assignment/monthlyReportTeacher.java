/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.lang.Integer.parseInt;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author ASUS
 */
public class monthlyReportTeacher implements ActionListener {

    private JFrame frame = new JFrame("Monthly Reports");
    private String teacherID, teacherName, comboValue;
    private JPanel headerPanel, bodyPanel;
    private JLabel headerTitle;
    private JButton report1Button, report2Button, returnButton;
    private JComboBox monthSelect;
    private String[] monthMonth = {"", "March", "April"};
    int[][] report1MarchData = new int[4][3];
    int[][] report1AprilData = new int[4][3];
    int[] report2MarchData = new int[4];
    int[] report2AprilData = new int[4];

    FetchData getData = new FetchData();
    private List<List<String>> scheduleRecord = getData.getBookingData();

    monthlyReportTeacher(String teacherID, String teacherName) {

        this.teacherID = teacherID;
        this.teacherName = teacherName;
        buildUI();

    }

    public void buildUI() {

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setTitle("Monthly Reports 1");
        frame.setResizable(false);

        headerTitle = new JLabel("Get Monthly Report ", JLabel.CENTER);
        frame.add(headerTitle, BorderLayout.CENTER);

        monthSelect = new JComboBox(monthMonth);
        monthSelect.setBounds(400, 50, 200, 50);
        frame.add(monthSelect);

        report1Button = new JButton("Report 1");
        report1Button.setBounds(400, 110, 200, 100);
        report1Button.addActionListener(this);
        frame.add(report1Button);

        report2Button = new JButton("Report 2");
        report2Button.setBounds(400, 220, 200, 100);
        report2Button.addActionListener(this);
        frame.add(report2Button);

        returnButton = new JButton("Return Home");
        returnButton.setBounds(10, 10, 150, 30);
        returnButton.addActionListener(this);
        frame.add(returnButton);

        frame.setSize(1000, 1000);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == report1Button) {
            comboValue = monthSelect.getSelectedItem().toString();

            if (comboValue == "March") {
                report1Button.setEnabled(true);
                for (int i = 0; i < scheduleRecord.size(); i++) {
                    if (scheduleRecord.get(i).get(3).contains("March") && scheduleRecord.get(i).get(6).equals("Attended")) {
                        if (scheduleRecord.get(i).get(2).equals("English")) {
                            report1MarchData[0][0]++;
                            report1MarchData[0][1] = report1MarchData[0][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Math")) {
                            report1MarchData[1][0]++;
                            report1MarchData[1][1] = report1MarchData[1][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Verbal Reasoning")) {
                            report1MarchData[2][0]++;
                            report1MarchData[2][1] = report1MarchData[2][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Non-Verbal Reasoning")) {
                            report1MarchData[3][0]++;
                            report1MarchData[3][1] = report1MarchData[3][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                    }
                }
                if (report1MarchData[0][0] != 0) {
                    report1MarchData[0][2] = report1MarchData[0][1] / report1MarchData[0][0];
                }
                if (report1MarchData[1][0] != 0) {
                    report1MarchData[1][2] = report1MarchData[1][1] / report1MarchData[1][0];
                }
                if (report1MarchData[2][0] != 0) {
                    report1MarchData[2][2] = report1MarchData[2][1] / report1MarchData[2][0];
                }
                if (report1MarchData[3][0] != 0) {
                    report1MarchData[3][2] = report1MarchData[3][1] / report1MarchData[3][0];
                }
                new MonthlyReport(teacherID, teacherName, report1MarchData, "March");
                frame.dispose();
                frame.dispose();
            }
            if (comboValue == "April") {
                report1Button.setEnabled(true);
                for (int i = 0; i < scheduleRecord.size(); i++) {
                    if (scheduleRecord.get(i).get(3).contains("April") && scheduleRecord.get(i).get(6).equals("Attended")) {
                        if (scheduleRecord.get(i).get(2).equals("English")) {
                            report1AprilData[0][0]++;
                            report1AprilData[0][1] = report1AprilData[0][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Math")) {
                            report1AprilData[1][0]++;
                            report1AprilData[1][1] = report1AprilData[1][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Verbal Reasoning")) {
                            report1AprilData[2][0]++;
                            report1AprilData[2][1] = report1AprilData[2][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Non-Verbal Reasoning")) {
                            report1AprilData[3][0]++;
                            report1AprilData[3][1] = report1AprilData[3][1] + parseInt(scheduleRecord.get(i).get(8));
                        }
                    }
                }
                if (report1AprilData[0][0] != 0) {
                    report1AprilData[0][2] = report1AprilData[0][1] / report1AprilData[0][0];
                }
                if (report1AprilData[1][0] != 0) {
                    report1AprilData[1][2] = report1AprilData[1][1] / report1AprilData[1][0];
                }
                if (report1AprilData[2][0] != 0) {
                    report1AprilData[2][2] = report1AprilData[2][1] / report1AprilData[2][0];
                }
                if (report1AprilData[3][0] != 0) {
                    report1AprilData[3][2] = report1AprilData[3][1] / report1AprilData[3][0];
                }
                new MonthlyReport(teacherID, teacherName, report1AprilData, "April");
                frame.dispose();

            }
            if (comboValue == "") {
                JOptionPane.showMessageDialog(null, "No Month Selected", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getSource() == report2Button) {
            comboValue = monthSelect.getSelectedItem().toString();
            if (comboValue == "March") {
                for (int i = 0; i < scheduleRecord.size(); i++) {
                    if (scheduleRecord.get(i).get(3).contains("March") && scheduleRecord.get(i).get(6).equals("Attended")) {
                        if (scheduleRecord.get(i).get(2).equals("English")) {
                            report2MarchData[0] = report2MarchData[0] + parseInt(scheduleRecord.get(i).get(5));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Math")) {
                            report2MarchData[1] = report2MarchData[1] + parseInt(scheduleRecord.get(i).get(5));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Verbal Reasoning")) {
                            report2MarchData[2] = report2MarchData[2] + parseInt(scheduleRecord.get(i).get(5));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Non-Verbal Reasoning")) {
                            report2MarchData[3] = report2MarchData[3] + parseInt(scheduleRecord.get(i).get(5));
                        }
                    }
                }

                int max = getIndex(report2MarchData);
                if (max == 0) {
                    System.out.println("English: " + report2MarchData[0]);
                    JOptionPane.showMessageDialog(null, "English has the most income in March.\n\nTotal Income: PKR" + report2MarchData[0], "English Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
                if (max == 1) {
                    System.out.println("Math: " + report2MarchData[1]);
                    JOptionPane.showMessageDialog(null, "Math has the most income in March.\n\nTotal Income: PKR" + report2MarchData[1], "Math Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
                if (max == 2) {
                    System.out.println("Verbal Reasoning: " + report2MarchData[2]);
                    JOptionPane.showMessageDialog(null, "Subject Verbal Reasoning has the most income in March.\n\nTotal Income: PKR" + report2MarchData[2], "Verbal Reasoning Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
                if (max == 3) {
                    System.out.println("Non-Verbal Reasoning: " + report2MarchData[3]);
                    JOptionPane.showMessageDialog(null, "Subject Non-Verbal Reasoning has the most income in March.\n\nTotal Income: PKR" + report2MarchData[3], "Non-Verbal Reasoning Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
            }
            if (comboValue == "April") {
                for (int i = 0; i < scheduleRecord.size(); i++) {
                    if (scheduleRecord.get(i).get(3).contains("April") && scheduleRecord.get(i).get(6).equals("Attended")) {
                        if (scheduleRecord.get(i).get(2).equals("English")) {
                            System.out.println(scheduleRecord.get(i).get(5));
                            report2AprilData[0] = report2AprilData[0] + parseInt(scheduleRecord.get(i).get(5));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Math")) {
                            report2AprilData[1] = report2AprilData[1] + parseInt(scheduleRecord.get(i).get(5));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Verbal Reasoning")) {
                            report2AprilData[2] = report2AprilData[2] + parseInt(scheduleRecord.get(i).get(5));
                        }
                        if (scheduleRecord.get(i).get(2).equals("Non-Verbal Reasoning")) {
                            report2AprilData[3] = report2AprilData[3] + parseInt(scheduleRecord.get(i).get(5));
                        }
                    }
                }

                int max = getIndex(report2AprilData);

                System.out.print(report2AprilData[0]);
                if (max == 0) {
                    System.out.println("English: " + report2AprilData[0]);
                    JOptionPane.showMessageDialog(null, "English has the most income in April.\n\nTotal Income: PKR" + report2AprilData[0], "Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
                if (max == 1) {
                    System.out.println("Math: " + report2AprilData[1]);
                    JOptionPane.showMessageDialog(null, "Math has the most income in April.\n\nTotal Income: PKR" + report2AprilData[1], "Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
                if (max == 2) {
                    System.out.println("Verbal Reasoning: " + report2AprilData[2]);
                    JOptionPane.showMessageDialog(null, "Subject Verbal Reasoning has the most income in April.\n\nTotal Income: PKR" + report2AprilData[2], "Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
                if (max == 3) {
                    System.out.println("Non-Verbal Reasoning: " + report2AprilData[3]);
                    JOptionPane.showMessageDialog(null, "Subject Non-Vebal Reasoning has the most income in April.\n\nTotal Income: PKR" + report2AprilData[3], "Income Report", JOptionPane.INFORMATION_MESSAGE);
                }
            }
            if (comboValue == "") {
                JOptionPane.showMessageDialog(null, "Please select a Month", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getSource() == returnButton) {
            new TeacherPage(teacherID, teacherName);
            frame.dispose();
        }

    }

    public int getIndex(int[] array) {
        if (array == null || array.length == 0) {
            return -1;
        }

        int largest = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > array[largest]) {
                largest = i;
            }
        }
        return largest;

    }
}
