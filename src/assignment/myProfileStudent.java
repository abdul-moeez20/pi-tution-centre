/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ASUS
 */
public class myProfileStudent implements ActionListener {

    private String studentID, studentName;
    private JFrame myProfileFrame;
    private JLabel message;
    private JTextField filter;
    private JButton backButton;
    private JTable table;
    private JScrollPane scroll;
    private DefaultTableModel tModel;
    private TableRowSorter sorter;

    public myProfileStudent(String studentID, String studentName) {

        this.studentID = studentID;
        this.studentName = studentName;

        myProfileFrame = new JFrame("My Profile" + studentName);
        myProfileFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myProfileFrame.setLayout(null);
        myProfileFrame.setSize(1000, 1000);
        myProfileFrame.setResizable(false);

        message = new JLabel("Overall Progress ", JLabel.CENTER);
        message.setBounds(0, 0, 1000, 50);
        myProfileFrame.add(message, BorderLayout.CENTER);

        backButton = new JButton("Back");
        backButton.setBounds(850, 610, 100, 30);
        backButton.addActionListener(this);

        String source = "programData\\bookings.csv";
        table = new JTable();
        scroll = new JScrollPane(table);
        String[] colNames = {"Lessons_ID", "Student_ID", "Subject", "Date", "Tutor", "Price", "Status", "StudentReview", "Rating", "Notes"};

        tModel = new DefaultTableModel(colNames, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        InputStream is;
        try {
            File f = new File(source);
            is = new FileInputStream(f);
            DataFromCSV(is);
            //table.getColumnModel().getColumn(0).setCellRenderer(new CustomCellRenderer());
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }

        sorter = new TableRowSorter(tModel);
        table.setModel(tModel);
        table.setRowSorter(sorter);

        filter = new JTextField();
        filter.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                FilterSettings();
            }

            public void insertUpdate(DocumentEvent e) {
                FilterSettings();
            }

            public void removeUpdate(DocumentEvent e) {
                FilterSettings();
            }
        }
        );

        JLabel filterLabel = new JLabel("Student ID");
        filterLabel.setBounds(50, 20, 100, 30);

        scroll.setBounds(50, 100, 900, 500);

        filter.setBounds(50, 50, 150, 30);
        filter.setText(studentID);
        filter.setEnabled(false);

        myProfileFrame.add(scroll, BorderLayout.CENTER);
        myProfileFrame.add(filterLabel);
        myProfileFrame.add(filter);
        myProfileFrame.add(scroll);
        myProfileFrame.add(backButton);
        myProfileFrame.setVisible(true);

    }

    private void FilterSettings() {
        RowFilter< DefaultTableModel, Object> studentIDF = null;
        //declare a row filter for your table model
        try {
            studentIDF = RowFilter.regexFilter("^" + filter.getText(), 1);
            //initialize with a regular expression
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(studentIDF);
    }

    void DataFromCSV(InputStream is) {

        Scanner scan = new Scanner(is);
        String[] array;
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.indexOf(",") > -1) {
                array = line.split(",");
            } else {
                array = line.split("\t");
            }
            Object[] data = new Object[array.length];
            for (int i = 0; i < array.length; i++) {
                data[i] = array[i];
            }

            tModel.addRow(data);
        }
        table.setModel(tModel);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == backButton) {
            new StudentPage(studentID, studentName);
            myProfileFrame.dispose();
        }

    }

}
