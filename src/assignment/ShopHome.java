/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author ASUS
 */
public class ShopHome implements ActionListener {

    String ShopKeeperID;
    JFrame ShopFrame = new JFrame("ShopKeeperPage");
    JLabel name = new JLabel("Hope Books Menu", JLabel.CENTER);
    JPanel panel1 = new JPanel();
    JButton books = new JButton("View Books List");
    JButton logOut = new JButton("LogOut");

    ShopHome(String ShopKeeperID) {

        this.ShopKeeperID = ShopKeeperID;

        ShopFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ShopFrame.setLayout(null);
        ShopFrame.add(panel1);
        ShopFrame.setResizable(false);
        ShopFrame.getContentPane().setBackground(new Color(250, 245, 228));
        ShopFrame.add(name, BorderLayout.CENTER);

        name.setBounds(0, 25, 700, 50);
        name.setFont(new Font("Acumin Variable Concept", Font.BOLD, 24));

        panel1.setLayout(new GridLayout(1, 1, 150, 150));
        panel1.setBounds(150, 100, 400, 100);
        panel1.add(books);

        books.addActionListener(this);

        logOut.setBounds(550, 250, 100, 30);
        logOut.setFont(new Font("Acumin Variable Concept", Font.BOLD, 14));
        logOut.addActionListener(this);
        ShopFrame.add(logOut);

        ShopFrame.setSize(700, 350);
        ShopFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == books) {
            new ShopBookList(ShopKeeperID);
            ShopFrame.dispose();
        }
        if (e.getSource() == logOut) {
            new HomePage();
            ShopFrame.dispose();
        }
    }
}
