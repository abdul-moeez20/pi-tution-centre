/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class AmendBookList implements ActionListener {

    private FetchData fetchData = new FetchData();
    private List<List<String>> booksData = fetchData.getBooksData();
    private List<List<String>> studentData = fetchData.getStudentData();
    private String teacherID, teacherName, studentName;
    private int rowSelected;
    private JFrame amendPageFrame = new JFrame("Amend Book List");
    private JLabel messageLabel;
    private JTextField bookNameField, bookSubject, studentID;
    private JScrollPane booksTablePane = new JScrollPane();
    private JTable booksTable = new JTable();
    private JButton requestCheckButton = new JButton("Check if Available");
    private JButton requestButton = new JButton("Request Book");
    private JButton backButton = new JButton("Return");

    public AmendBookList(String teacherID, String teacherName) {

        this.teacherID = teacherID;
        this.teacherName = teacherName;

        amendPageFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        amendPageFrame.setLayout(null);
        amendPageFrame.setResizable(false);
        amendPageFrame.getContentPane().setBackground(new Color(250, 245, 228));

        messageLabel = new JLabel("Welcome " + teacherName + ". Please select a Book from the Table Below", JLabel.CENTER);
        messageLabel.setBounds(0, 0, 1000, 50);
        amendPageFrame.add(messageLabel, BorderLayout.CENTER);

        booksTablePane.setBounds(50, 50, 900, 300);
        amendPageFrame.add(booksTablePane);

        JLabel lessonDetails = new JLabel("Book Details:");
        lessonDetails.setBounds(57, 293, 220, 150);

        JLabel box = new JLabel();
        box.setBounds(57, 382, 207, 47);
        box.setBackground(Color.white);
        box.setForeground(Color.white);
        box.setBorder(BorderFactory.createLineBorder(Color.black, 3));

        bookNameField = new JTextField();
        bookSubject = new JTextField();
        studentID = new JTextField();

        bookNameField.setEditable(false);
        bookSubject.setEditable(false);

        bookNameField.setBounds(60, 385, 200, 20);
        bookSubject.setBounds(60, 405, 200, 20);
        JLabel enterID = new JLabel("Enter Student ID");
        enterID.setBounds(500, 375, 125, 30);
        studentID.setBounds(625, 375, 100, 30);

        requestCheckButton.setBounds(750, 375, 200, 30);
        requestCheckButton.addActionListener(this);

        requestButton.setBounds(750, 410, 200, 30);
        requestButton.setVisible(false);
        requestButton.addActionListener(this);

        backButton.setBounds(850, 500, 100, 30);
        backButton.addActionListener(this);

        booksTable.setModel(new DefaultTableModel(
                new Object[][]{
                    //Book List
                    {"English Comprehension 1", "English"},
                    {"English Made Easy", "English"},
                    {"Cutting Edge", "English"},
                    {"Basic Mathematics", "Math"},
                    {"Mental Math 1", "Math"},
                    {"Advanced Calculus", "Math"},
                    {"Verbal Reasoning Assessment Papers", "Verbal Reasoning"},
                    {"CEM Verbal Reasoning Practice Book", "Verbal Reasoning"},
                    {"Non-Verbal Reasoning Study Book", "NonVerbal Reasoning "},
                    {"CEM Non-Verbal Reasoning Practice Book", "NonVerbal Reasoning "},},
                new String[]{"BookName", "Subject"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        }
        );

        booksTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lessonsTableMouseClicked(evt);
            }
        });

        booksTablePane.setViewportView(booksTable);

        amendPageFrame.add(lessonDetails);
        amendPageFrame.add(box);
        amendPageFrame.add(enterID);
        amendPageFrame.add(studentID);
        amendPageFrame.add(backButton);
        amendPageFrame.add(bookNameField);
        amendPageFrame.add(bookSubject);
        amendPageFrame.add(requestButton);
        amendPageFrame.add(requestCheckButton);
        amendPageFrame.setSize(1000, 600);
        amendPageFrame.setVisible(true);

    }

    //MouseClick Event for lessonsTable
    private void lessonsTableMouseClicked(MouseEvent evt) {

        DefaultTableModel tableModel = (DefaultTableModel) booksTable.getModel();
        rowSelected = booksTable.getSelectedRow();

        bookNameField.setText(tableModel.getValueAt(rowSelected, 0).toString());
        bookSubject.setText(tableModel.getValueAt(rowSelected, 1).toString());
        requestButton.setVisible(false);
        studentName = "";
        studentID.setText("");

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == requestCheckButton) {

            for (int i = 0; i < booksData.size(); i++) {

                if (booksData.get(i).get(0).equals(bookNameField.getText()) && booksData.get(i).get(2).equals(studentID.getText())) {
                    JOptionPane.showMessageDialog(null, "Book Already Requested!!");
                    new AmendBookList(teacherID, teacherName);
                    amendPageFrame.dispose();
                    break;
                } else {
                    requestButton.setVisible(true);
                }
            }

            if (bookNameField.getText().isEmpty() || studentID.getText().isEmpty()) {
                new BooksPage(teacherID, teacherName);
                amendPageFrame.dispose();
            }

        }

        if (e.getSource() == requestButton) {

            String name = bookNameField.getText();
            String subject = bookSubject.getText();

            try {
                FileWriter fw = new FileWriter("programData\\booksData.csv", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);

                for (int i = 0; i < studentData.size(); i++) {
                    if (studentData.get(i).get(0).equals(studentID.getText())) {
                        studentName = studentData.get(i).get(1);
                    }
                }

                pw.println(name + "," + subject + "," + studentID.getText() + "," + studentName + "," + teacherName);
                pw.flush();
                pw.close();
                JOptionPane.showMessageDialog(null, "Book Requested :)");
                new AmendBookList(teacherID, teacherName);

            } catch (IOException ex) {
            }
        }

        if (e.getSource() == backButton) {
            new TeacherPage(teacherID, teacherName);
            amendPageFrame.dispose();
        }

    }

}
