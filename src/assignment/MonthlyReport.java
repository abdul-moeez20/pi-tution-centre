/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class MonthlyReport extends JFrame implements ActionListener {

    //Initializing and Declaring Variables
    JFrame frame = new JFrame();
    int[][] report1Data = new int[4][3];
    private String teacherID, teacherName, monthName;
    private int indexNumber;
    private int rowNumber = 0;
    private JScrollPane scrollPane;
    private JLabel headerTitle;
    private JButton returnButton;
    private JTable table;

    public MonthlyReport(String teacherID, String teacherName, int[][] report1Data, String monthName) {

        this.report1Data = report1Data;
        this.teacherID = teacherID;
        this.teacherName = teacherName;
        this.monthName = monthName;
        //Call createUI to create a UserInterface for user
        createUI();

    }

    private void createUI() {

        //Frame Settings
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setTitle(monthName + " Lessons Report");

        //HeaderPanel Settings
        headerTitle = new JLabel(monthName + " Lessons Report", JLabel.CENTER);
        headerTitle.setBounds(0, 0, 1000, 50);
        frame.add(headerTitle, BorderLayout.CENTER);

        //Adding ScrollPane to BodyPanel
        scrollPane = new JScrollPane();
        scrollPane.setBounds(60, 50, 900, 500);
        scrollPane.setBackground(Color.WHITE);
        frame.add(scrollPane);

        //Creating a Table to Display hardcoded Book List
        table = new JTable();
        table.setModel(new DefaultTableModel(
                new Object[][]{
                    //row data
                    {"English", report1Data[0][0], report1Data[0][2]},
                    {"Math", report1Data[1][0], report1Data[1][2]},
                    {"Verbal Reasoning", report1Data[2][0], report1Data[2][2]},
                    {"Non-Verbal Reasoning", report1Data[3][0], report1Data[3][2]},},
                //column Names
                new String[]{"Subject", "Number of Student", "Average Rating"}
        ) {
            //Disable Editing
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        }
        );
        //Add Table to ViewPort
        scrollPane.setViewportView(table);

        returnButton = new JButton("Back");
        returnButton.setFocusable(false);
        returnButton.setBounds(50, 600, 200, 50);
        returnButton.addActionListener(this);
        frame.add(returnButton);

        //Final Frame Settings
        frame.setSize(1000, 1000);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == returnButton) {
            new monthlyReportTeacher(teacherID, teacherName);
            this.dispose();
        }
    }
}
