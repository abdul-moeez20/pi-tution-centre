/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class CreateAccount implements ActionListener {

    FetchData fetchData = new FetchData();
    List<List<String>> studentData = fetchData.getStudentData();

    JFrame createAccountFrame = new JFrame("PI Tution Centre (PTC)/ SignUp");

    JLabel id = new JLabel("ID: ");
    JLabel name = new JLabel("Name: ");
    JLabel gender = new JLabel("Gender: ");
    JLabel dob = new JLabel("DOB: ");
    JLabel address = new JLabel("Address: ");
    JLabel emergencyNumber = new JLabel("Emergency Phone Number: ");

    JTextField idField = new JTextField();
    JTextField nameField = new JTextField();
    JTextField genderField = new JTextField();
    JTextField dobField = new JTextField();
    JTextField addressField = new JTextField();
    JTextField emergencyNumberField = new JTextField();

    JButton create = new JButton("Create");

    CreateAccount() {

        createAccountFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        createAccountFrame.setLayout(new GridLayout(14, 1));

        idField.setEditable(false);
        idField.setText("S" + studentData.size());
        createAccountFrame.add(id);
        createAccountFrame.add(idField);
        createAccountFrame.add(name);
        createAccountFrame.add(nameField);
        createAccountFrame.add(gender);
        createAccountFrame.add(genderField);
        createAccountFrame.add(dob);
        createAccountFrame.add(dobField);
        createAccountFrame.add(address);
        createAccountFrame.add(addressField);
        createAccountFrame.add(emergencyNumber);
        createAccountFrame.add(emergencyNumberField);
        createAccountFrame.add(new JLabel(""));
        createAccountFrame.add(create);
        create.addActionListener(this);

        createAccountFrame.setResizable(false);
        createAccountFrame.setSize(500, 500);
        createAccountFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == create) {
            try {
                try (FileWriter myWriter = new FileWriter("programData\\studentData.csv", true);
                        BufferedWriter b = new BufferedWriter(myWriter);
                        PrintWriter p = new PrintWriter(b);) {
                    p.println(idField.getText() + "," + nameField.getText() + "," + genderField.getText() + "," + dobField.getText() + "," + addressField.getText() + "," + emergencyNumberField.getText());
                    p.flush();
                    p.close();
                }
                System.out.println("Successfully wrote to the file.");
                createAccountFrame.dispose();
                new HomePage();
            } catch (IOException e1) {
                System.out.println("An error occurred.");
            }
        }
    }

}
