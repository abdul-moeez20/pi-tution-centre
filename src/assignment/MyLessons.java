/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ASUS
 */
public class MyLessons implements ActionListener {

    private String teacherID, teacherName;
    private JFrame myLessonsFrame;
    private JLabel message;
    private JTextField filterName;
    private JButton backButton;
    private JTable table;
    private JScrollPane scroll;
    private DefaultTableModel tModel;
    private TableRowSorter sorter;

    //GetData
    private FetchData getData = new FetchData();
    private List<List<String>> teachersData = getData.getTeacherData();
    private List<List<String>> lessonsData = getData.getLessonsData();

    public MyLessons(String teacherID, String teacherName) {

        this.teacherID = teacherID;
        this.teacherName = teacherName;

        myLessonsFrame = new JFrame("Lessons for " + teacherName);
        myLessonsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myLessonsFrame.setLayout(null);
        myLessonsFrame.setSize(1000, 1000);
        myLessonsFrame.setResizable(false);

        message = new JLabel("Lessons for " + teacherName, JLabel.CENTER);
        message.setBounds(0, 0, 1000, 50);
        myLessonsFrame.add(message, BorderLayout.CENTER);

        backButton = new JButton("Back");
        backButton.setBounds(850, 610, 100, 30);
        backButton.addActionListener(this);

        String source = "programData\\LessonsData.csv";
        table = new JTable();
        scroll = new JScrollPane(table);
        String[] colNames = {"LessonID", "Subject", "Week", "Time", "Date", "Tutor", "Price"};

        tModel = new DefaultTableModel(colNames, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        InputStream is;
        try {
            File f = new File(source);
            is = new FileInputStream(f);
            DataFromCSV(is);
            //table.getColumnModel().getColumn(0).setCellRenderer(new CustomCellRenderer());
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }

        sorter = new TableRowSorter(tModel);
        table.setModel(tModel);
        table.setRowSorter(sorter);

        filterName = new JTextField(teacherName);
        filterName.setEnabled(false);
        filterName.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                FilterSettings();
            }

            public void insertUpdate(DocumentEvent e) {
                FilterSettings();
            }

            public void removeUpdate(DocumentEvent e) {
                FilterSettings();
            }
        }
        );

        JLabel filterLabel = new JLabel();
        filterLabel.setBounds(50, 20, 100, 30);

        scroll.setBounds(50, 100, 900, 500);

        filterName.setBounds(50, 50, 150, 30);

        filterName.setText(teacherName);
        myLessonsFrame.add(scroll, BorderLayout.CENTER);
        myLessonsFrame.add(filterLabel);
        myLessonsFrame.add(filterName);
        myLessonsFrame.add(scroll);
        myLessonsFrame.add(backButton);
        myLessonsFrame.setVisible(true);

    }

    private void FilterSettings() {
        RowFilter< DefaultTableModel, Object> bookName = null;
        //declare a row filter for your table model
        try {
            bookName = RowFilter.regexFilter("^" + filterName.getText(), 5);
            //initialize with a regular expression
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(bookName);
    }

    void DataFromCSV(InputStream is) {

        Scanner scan = new Scanner(is);
        String[] array;
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.indexOf(",") > -1) {
                array = line.split(",");
            } else {
                array = line.split("\t");
            }
            Object[] data = new Object[array.length];
            for (int i = 0; i < array.length; i++) {
                data[i] = array[i];
            }

            tModel.addRow(data);
        }
        table.setModel(tModel);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == backButton) {
            new TeacherPage(teacherID, teacherName);
            myLessonsFrame.dispose();
        }
    }
}
