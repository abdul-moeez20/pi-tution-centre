/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class HomePage implements ActionListener {

    JFrame signInFrame = new JFrame("PI Tution Centre (PTC)");
    JLabel signin = new JLabel("Use ID to Sign-In");
    JLabel name = new JLabel("Pi-Tution Centre(PTC)");
    JTextField idField = new JTextField();
    JButton next = new JButton("Next");
    JButton signUp = new JButton("Sign Up");
    String[] choice = {"Student", "Teacher", "Shop Keeper"};
    JComboBox choiceBox = new JComboBox(choice);

    FetchData fetchData = new FetchData();
    List<List<String>> teacherData = fetchData.getTeacherData();
    List<List<String>> studentData = fetchData.getStudentData();

    HomePage() {

        signInFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        signInFrame.setLayout(null);
        signInFrame.getContentPane().setBackground(new Color(250, 245, 228));
        signInFrame.add(signin);
        signInFrame.add(idField);
        signInFrame.add(next);
        signInFrame.add(signUp);
        signInFrame.add(choiceBox);
        signInFrame.add(name);

        name.setBounds(120, 50, 300, 50);
        name.setFont(new Font("Acumin Variable Concept", Font.BOLD, 24));

        signin.setBounds(100, 190, 300, 30);
        signin.setFont(new Font("Acumin Variable Concept", Font.PLAIN, 16));

        idField.setBounds(100, 225, 300, 30);
        idField.setFont(new Font("Acumin Variable Concept", Font.PLAIN, 16));

        choiceBox.setBounds(100, 260, 200, 30);
        choiceBox.setFont(new Font("Acumin Variable Concept", Font.PLAIN, 16));

        next.setBounds(310, 260, 90, 30);
        next.setFont(new Font("Acumin Variable Concept", Font.BOLD, 14));
        next.addActionListener(this);

        signUp.setBounds(310, 300, 90, 30);
        signUp.setFont(new Font("Acumin Variable Concept", Font.BOLD, 14));
        signUp.addActionListener(this);

        signInFrame.setResizable(false);
        signInFrame.setSize(500, 500);
        signInFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == next) {

            if (choiceBox.getSelectedIndex() == 0) {

                for (int i = 0; i < studentData.size(); i++) {
                    if (studentData.get(i).get(0).equals(idField.getText())) {
                        new StudentPage(studentData.get(i).get(0), studentData.get(i).get(1));
                        signInFrame.dispose();
                    }
                }
            } else if (choiceBox.getSelectedIndex() == 1) {

                for (int i = 0; i < teacherData.size(); i++) {
                    if (teacherData.get(i).get(0).equals(idField.getText())) {
                        new TeacherPage(teacherData.get(i).get(0), teacherData.get(i).get(1));
                        signInFrame.dispose();
                    }
                }
            } else if (choiceBox.getSelectedIndex() == 2) {
                if (idField.getText().equals("HopeBook")) {
                    new ShopHome("HopeBook");
                    signInFrame.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "ShopKeeper Login Unsucessfull");
                }
            }
        }

        if (e.getSource() == signUp) {
            new CreateAccount();
            signInFrame.dispose();
        }
    }

}
