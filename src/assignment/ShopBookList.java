/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ASUS
 */
public class ShopBookList implements ActionListener {

    private String shopKeeperID;
    private JFrame booksFrame;
    private JLabel message;
    private JTextField filter;
    private JButton backButton;
    private JTable table;
    private JScrollPane scroll;
    private DefaultTableModel tModel;
    private TableRowSorter sorter;
    private String column[] = {"BookID", "StudentID", "StudentName", "BookName"};

    //GetData
    private FetchData getData = new FetchData();
    private List<List<String>> booksRecord = getData.getBooksData();

    public ShopBookList(String shopKeeperID) {

        this.shopKeeperID = shopKeeperID;

        booksFrame = new JFrame("Shop Keeper Book List");
        booksFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        booksFrame.setLayout(null);
        booksFrame.setSize(1000, 1000);
        booksFrame.setResizable(false);

        message = new JLabel("Books Requested by Students ", JLabel.CENTER);
        message.setBounds(0, 0, 1000, 50);
        booksFrame.add(message, BorderLayout.CENTER);

        backButton = new JButton("Back");
        backButton.setBounds(850, 610, 100, 30);
        backButton.addActionListener(this);

        String source = "programData\\booksData.csv";
        table = new JTable();
        scroll = new JScrollPane(table);
        String[] colNames = {"BookName", "Subject", "StudentID", "StudentName", "AddedByTutor"};

        tModel = new DefaultTableModel(colNames, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        InputStream is;
        try {
            File f = new File(source);
            is = new FileInputStream(f);
            DataFromCSV(is);
            //table.getColumnModel().getColumn(0).setCellRenderer(new CustomCellRenderer());
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }

        sorter = new TableRowSorter(tModel);
        table.setModel(tModel);
        table.setRowSorter(sorter);

        filter = new JTextField();
        filter.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                FilterSettings();
            }

            public void insertUpdate(DocumentEvent e) {
                FilterSettings();
            }

            public void removeUpdate(DocumentEvent e) {
                FilterSettings();
            }
        }
        );

        JLabel filterLabel = new JLabel("Student ID:");
        filterLabel.setBounds(50, 20, 100, 30);

        scroll.setBounds(50, 100, 900, 500);

        filter.setBounds(50, 50, 150, 30);

        booksFrame.add(scroll, BorderLayout.CENTER);
        booksFrame.add(filterLabel);
        booksFrame.add(filter);
        booksFrame.add(scroll);
        booksFrame.add(backButton);
        booksFrame.setVisible(true);

    }

    private void FilterSettings() {
        RowFilter< DefaultTableModel, Object> bookName = null;
        //declare a row filter for your table model
        try {
            bookName = RowFilter.regexFilter("^" + filter.getText(), 2);
            //initialize with a regular expression
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(bookName);
    }

    void DataFromCSV(InputStream is) {

        Scanner scan = new Scanner(is);
        String[] array;
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.indexOf(",") > -1) {
                array = line.split(",");
            } else {
                array = line.split("\t");
            }
            Object[] data = new Object[array.length];
            for (int i = 0; i < array.length; i++) {
                data[i] = array[i];
            }

            tModel.addRow(data);
        }
        table.setModel(tModel);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == backButton) {
            new ShopHome(shopKeeperID);
            booksFrame.dispose();
        }
    }
}
