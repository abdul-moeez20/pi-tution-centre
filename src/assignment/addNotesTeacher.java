/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ASUS
 */
public class addNotesTeacher implements ActionListener {

    private JFrame frame = new JFrame();
    private String teacherID, teacherName, lessonID, studentID, notes;
    private int selectedRow;
    private JLabel title;
    private JTextField rowIndex, status;
    private JButton returnB, notesB;
    private JTable table;
    private JScrollPane panel;
    private DefaultTableModel dtModel;
    private TableRowSorter filter;

    //GetData
    private FetchData FetchData = new FetchData();
    private List<List<String>> bookingData = FetchData.getBookingData();

    public addNotesTeacher(String teacherID, String teacherName) {

        this.teacherID = teacherID;
        this.teacherName = teacherName;

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setTitle("Add Note");

        title = new JLabel("Add recommendation for Student: ", JLabel.CENTER);
        title.setBounds(0, 0, 1000, 50);
        frame.add(title, BorderLayout.CENTER);

        String source = "programData\\bookings.csv";
        table = new JTable();
        panel = new JScrollPane(table);
        String[] colNames = {"Lessons_ID", "Student_ID", "Subject", "Date", "Tutor", "Price", "Status", "StudentReview", "Rating", "Notes"};
        dtModel = new DefaultTableModel(colNames, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        InputStream is;
        try {
            File f = new File(source);
            is = new FileInputStream(f);
            insertData(is);
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(frame, ioe, "Error reading data", JOptionPane.ERROR_MESSAGE);
        }
        filter = new TableRowSorter(dtModel);
        table.setModel(dtModel);
        table.setRowSorter(filter);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MouseClick(evt);
            }
        });
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                int viewRow = table.getSelectedRow();
                if (viewRow < 0) {
                    //Selection got filtered away.
                    rowIndex.setText("");
                } else {
                    selectedRow
                            = table.convertRowIndexToModel(viewRow);
                    rowIndex.setText(String.format("%s", selectedRow));
                }
            }
        }
        );

        rowIndex = new JTextField();
        status = new JTextField();
        status.setEditable(false);
        status.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                filterAttendedLessons();
            }

            public void insertUpdate(DocumentEvent e) {
                filterAttendedLessons();
            }

            public void removeUpdate(DocumentEvent e) {
                filterAttendedLessons();
            }
        }
        );
        panel.setBounds(50, 50, 900, 500);
        status.setBounds(50, 10, 100, 30);

        notesB = new JButton("Add Notes");
        notesB.setBounds(800, 600, 150, 30);
        notesB.addActionListener(this);
        notesB.setEnabled(true);

        returnB = new JButton("Return");
        returnB.setBounds(50, 600, 200, 30);
        returnB.addActionListener(this);
        status.setText("Attended");
        status.setEditable(true);

        frame.add(rowIndex);
        frame.add(status);
        frame.add(panel);
        frame.add(returnB);
        frame.add(notesB);

        frame.setSize(1000, 1000);
        frame.setResizable(false);
        frame.setVisible(true);

    }

    private void insertData(InputStream is) {

        Scanner scan = new Scanner(is);
        String[] array;
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.indexOf(",") > -1) {
                array = line.split(",");
            } else {
                array = line.split("\t");
            }
            Object[] data = new Object[array.length];
            for (int i = 0; i < array.length; i++) {
                data[i] = array[i];
            }

            dtModel.addRow(data);
        }
        table.setModel(dtModel);

    }

    private void filterAttendedLessons() {

        RowFilter< DefaultTableModel, Object> statusField = null;
        //declare a row filter for your table model
        try {
            statusField = RowFilter.regexFilter("^" + status.getText(), 6);
            //initialize with a regular expression
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        filter.setRowFilter(statusField);
    }

    private void MouseClick(MouseEvent evt) {

        lessonID = bookingData.get(selectedRow).get(0);
        studentID = bookingData.get(selectedRow).get(1);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == notesB) {
            notes = JOptionPane.showInputDialog("Please add recomendations");
            System.out.println(lessonID);
            System.out.println(bookingData);
            System.out.println(bookingData.size());
            for (int i = 0; i < bookingData.size(); i++) {
                if (bookingData.get(i).get(0).equals(lessonID) && bookingData.get(i).get(1).equals(studentID)) {

                    List<String> temp = bookingData.get(i);
                    temp.set(9, notes);
                    bookingData.set(i, temp);
                    break;
                }
            }

            System.out.println(bookingData);

            try {
                FileWriter fw = new FileWriter("programData\\bookings.csv");
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);

                for (int c = 0; c < bookingData.size(); c++) {
                    pw.println(bookingData.get(c).get(0) + "," + bookingData.get(c).get(1) + "," + bookingData.get(c).get(2)
                            + "," + bookingData.get(c).get(3) + "," + bookingData.get(c).get(4) + "," + bookingData.get(c).get(5) + ","
                            + bookingData.get(c).get(6) + "," + bookingData.get(c).get(7) + "," + bookingData.get(c).get(8) + ","
                            + bookingData.get(c).get(9));
                }
                pw.flush();
                pw.close();
                JOptionPane.showMessageDialog(null, "Success");
                new TeacherPage(teacherID, teacherName);

            } catch (IOException ex) {
            }

            new TeacherPage(teacherID, teacherName);
            frame.dispose();

        }

        if (e.getSource() == returnB) {
            new TeacherPage(teacherID, teacherName);
            frame.dispose();
        }

    }

}
