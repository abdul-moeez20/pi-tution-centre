/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class FetchData {

    private List<List<String>> studentData = new ArrayList<List<String>>();
    private List<List<String>> teacherData = new ArrayList<List<String>>();
    private List<List<String>> bookingData = new ArrayList<List<String>>();
    private List<List<String>> booksData = new ArrayList<List<String>>();
    private List<List<String>> lessonsData = new ArrayList<List<String>>();

    public List<List<String>> getStudentData() {

        BufferedReader br = null;
        String line = "";
        int size;

        try {
            br = new BufferedReader(new FileReader("programData\\studentData.csv"));

            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                List<String> row1 = Arrays.asList(row);
                studentData.add(row1);
            }
            size = studentData.size();
        } catch (Exception e) {
        }
        return studentData;
    }

    public List<List<String>> getTeacherData() {

        BufferedReader br = null;
        String line = "";
        int size;

        try {
            br = new BufferedReader(new FileReader("programData\\teacherData.csv"));

            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                List<String> row1 = Arrays.asList(row);
                teacherData.add(row1);
            }
            size = teacherData.size();
        } catch (Exception e) {
        }
        return teacherData;
    }

    public List<List<String>> getBookingData() {

        BufferedReader br = null;
        String line = "";
        int size;

        try {
            br = new BufferedReader(new FileReader("programData\\bookings.csv"));

            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                List<String> row1 = Arrays.asList(row);
                bookingData.add(row1);
            }
            size = bookingData.size();
        } catch (Exception e) {
        }
        return bookingData;
    }

    public List<List<String>> getBooksData() {

        BufferedReader br = null;
        String line = "";
        int size;

        try {

            br = new BufferedReader(new FileReader("programData\\booksData.csv"));

            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                List<String> row1 = Arrays.asList(row);
                booksData.add(row1);
            }
            size = booksData.size();
        } catch (Exception e) {
        }
        return booksData;
    }

    public List<List<String>> getLessonsData() {

        BufferedReader br = null;
        String line = "";
        int size;

        try {

            br = new BufferedReader(new FileReader("programData\\booksData.csv"));

            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                List<String> row1 = Arrays.asList(row);
                lessonsData.add(row1);
            }
            size = lessonsData.size();
        } catch (Exception e) {
        }
        return lessonsData;
    }

}
